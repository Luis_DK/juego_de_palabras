  package com.example.juego_de_palabras;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

  public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button inicio=(Button)findViewById(R.id.btnAgregar);
        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //se llama lla funcion con todos los elementos de la misma
                lol();
            }
        });

    }
    public void lol()
    {
        RadioButton Hombre=(RadioButton)findViewById(R.id.RBHombre);//Se crea una variable de tipo radiobutton
        RadioButton Mujer=(RadioButton)findViewById(R.id.RBMujer);//Se crea una variable de tipo radiobutton


        if(Hombre.isChecked())
        {
            //elementos fundamendales para poder crear nombres y numeros aleatorios
            Random r = new Random();// se crea una variable random para utilizar elementos aleatorios
            String[] NombresMujeres={"Sofia","Andrea","Mariana","Paulina","Nancy","Daniela"};//se crea un arreglo de tipo string con datos
            int Numerobra = r.nextInt(NombresMujeres.length);//se crea un variable tipo entero para poder leer el arreglo y esocojer uno de los elementos alearotiamente
            String [] Hijos={"1","2","3","4","5","6","7","8","9"};//se crea un arreglo ahora de numeros osea los hijos
            int morros=r.nextInt(Hijos.length);//se crea un variable tipo entero para poder leer el arreglo y esocojer uno de los elementos alearotiamente
            //se crean variables de tipo EditText
            EditText nombre=(EditText)findViewById(R.id.editTextNombre);
            EditText apellido=(EditText)findViewById(R.id.editTextApellido);
            EditText edades=(EditText)findViewById(R.id.editTextEdad);
            //se generan variables tipo String para obtener lo que alla en el EditText y dandoles una nueva variable
            String nom =nombre.getText().toString();
            String ape = apellido.getText().toString();
            String edad= edades.getText().toString();

            // toda esta seccion se manda imprimir los datos obtenidos diciendo el nombre,apellido,edad,con quien te casaras y al final cuantos hijos tendra
            Toast.makeText(this, "Tu nombre es: " +nom, Toast.LENGTH_LONG).show();

            Toast.makeText(this, "Tu apellido es: "+ape, Toast.LENGTH_LONG).show();

            Toast.makeText(this, "Tu edad es: "+edad, Toast.LENGTH_LONG).show();

            Toast.makeText(this, "Te casaras con: "+NombresMujeres[Numerobra], Toast.LENGTH_LONG).show();

            Toast.makeText(this, "Los hijos que tendras: "+Hijos[morros], Toast.LENGTH_SHORT).show();
        }

        if (Mujer.isChecked())
        {
            //elementos fundamendales para poder crear nombres y numeros aleatorios
            Random r = new Random();// se crea una variable random para utilizar elementos aleatorios
            String[] NombresHombres={"Ulises","Obed","Omar","Alfredo","Roberto","Gabriel"};//se crea un arreglo de tipo string con datos
            int Numerobra = r.nextInt(NombresHombres.length);//se crea un variable tipo entero para poder leer el arreglo y esocojer uno de los elementos alearotiamente
            String [] Hijos={"1","2","3","4","5","6","7","8","9"};//se crea un arreglo ahora de numeros osea los hijos
            int morros=r.nextInt(Hijos.length);//se crea un variable tipo entero para poder leer el arreglo y esocojer uno de los elementos alearotiamente
            //se crean variables de tipo EditText
            EditText nombre=(EditText)findViewById(R.id.editTextNombre);
            EditText apellido=(EditText)findViewById(R.id.editTextApellido);
            EditText edades=(EditText)findViewById(R.id.editTextEdad);

            //se generan variables tipo String para obtener lo que alla en el EditText y dandoles una nueva variable
            String nom =nombre.getText().toString();
            String ape = apellido.getText().toString();
            String edad= edades.getText().toString();

            // toda esta seccion se manda imprimir los datos obtenidos diciendo el nombre,apellido,edad,con quien te casaras y al final cuantos hijos tendra
            Toast.makeText(this, "Tu nombre es: " +nom, Toast.LENGTH_LONG).show();

            Toast.makeText(this, "Tu apellido es: "+ape, Toast.LENGTH_LONG).show();

            Toast.makeText(this, "Tu edad es: "+edad, Toast.LENGTH_LONG).show();

            Toast.makeText(this, "Te casaras con: "+NombresHombres[Numerobra], Toast.LENGTH_LONG).show();

            Toast.makeText(this, "Los hijos que tendras: "+Hijos[morros], Toast.LENGTH_SHORT).show();

        }

    }
}
